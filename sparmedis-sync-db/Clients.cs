﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sparmedis_sync_db
{
    public class Client
    {
        public string id;
        public string codigo;
        public string nome;
        public string nif;
        public string responsavel;
        public string morada;
        public string email;
        public string local;
        public string cdpostal;
        public string tppreco;

        public string action;

        public Client()
        {
            // TODO: Complete member initialization
        }

    }
    public class Clients
    {
        public static Hashtable list = new Hashtable();
        public static Hashtable db_list = new Hashtable();
        public static bool updated_db = false;
        public static bool updated_ws = false;

        public static Label o_sync_status;

        public static void sync()
        {
            Hashtable tmp = new Hashtable();
            foreach (DictionaryEntry entry in list)
            {
                Client client = entry.Value as Client;
                client.action = "";
                if (!db_list.ContainsKey(client.codigo))
                {
                    client.action = "d";
                }
                tmp[client.codigo] = client;
            }
            foreach (DictionaryEntry entry in db_list)
            {
                Client client = entry.Value as Client;
                client.action = "";
                if (!list.ContainsKey(client.codigo))
                {
                    client.action = "a";
                }
                else
                {
                    Client l_client = list[client.codigo.ToString()] as Client;
                    if (!l_client.nome.Equals(client.nome) && client.action.Equals("")) { client.action = "u"; }
                    if (!l_client.nif.Equals(client.nif) && client.action.Equals("")) { client.action = "u"; }
                    if (!l_client.responsavel.Equals(client.responsavel) && client.action.Equals("")) { client.action = "u"; }
                    if (!l_client.morada.Equals(client.morada) && client.action.Equals("")) { client.action = "u"; }
                    if (!l_client.email.Equals(client.email) && client.action.Equals("")) { client.action = "u"; }
                    if (!l_client.local.Equals(client.local) && client.action.Equals("")) { client.action = "u"; }
                    if (!l_client.cdpostal.Equals(client.cdpostal) && client.action.Equals("")) { client.action = "u"; }
                    if (!l_client.tppreco.Equals(client.tppreco) && client.action.Equals("")) { client.action = "u"; }
                }
                tmp[client.codigo] = client;
            }
            list = new Hashtable();
            db_list = new Hashtable();
            db_list = tmp;
            return;
        }

        public static bool update()
        {
            if (Clients.updated_db)
            {
                int counter = 0;
                string clients_str = "";
                foreach (DictionaryEntry entry in Clients.db_list)
                {
                    Client client = entry.Value as Client;
                    if (client.action.Equals("")) continue;
                    if (counter == 0) clients_str += "&clients=";
                    else clients_str += "_spout_";
                    clients_str += WebUtility.UrlEncode(client.codigo) + "_spin_";
                    clients_str += WebUtility.UrlEncode(client.nome) + "_spin_";
                    clients_str += WebUtility.UrlEncode(client.nif) + "_spin_";
                    clients_str += WebUtility.UrlEncode(client.responsavel) + "_spin_";
                    clients_str += WebUtility.UrlEncode(client.morada) + "_spin_";
                    clients_str += WebUtility.UrlEncode(client.email) + "_spin_";
                    clients_str += WebUtility.UrlEncode(client.local) + "_spin_";
                    clients_str += WebUtility.UrlEncode(client.cdpostal) + "_spin_";
                    clients_str += WebUtility.UrlEncode(client.tppreco) + "_spin_";
                    clients_str += WebUtility.UrlEncode(client.action);
                    counter += 1;
                }
                if (!updated_ws)
                {
                    clients_str += "&initial_update=1";
                    updated_ws = true;
                }
                if (counter != 0)
                {
                    //Utils.log(clients_str);
                    string retval = ws.post("clients", clients_str);
                    if (retval != "error")
                    {
                        list = new Hashtable();
                        foreach (DictionaryEntry entry in Clients.db_list)
                        {
                            Client client = entry.Value as Client;
                            if (client.action.Equals("d")) continue;
                            list[client.codigo.ToString()] = client;
                        }
                        o_sync_status.Text = counter + " clientes sincronizados";
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    list = db_list;
                    o_sync_status.Text = "Clientes sem alterações";
                    return true;
                }
            }
            else
            {
                return false;
            }
            return true;
        }
    }
}
