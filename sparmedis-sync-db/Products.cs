﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sparmedis_sync_db
{
    public class Product
    {
        public string id;
        public string tipo_codigo;
        public string codigo;
        public string nome;
        public string preco;
        public string bonus;
        public string iva;
        public string familia;
        public string pvp3siva;
        public string pvalsiva;

        public string action;

        public Product()
        {
            // TODO: Complete member initialization
        }

    }
    public class Products
    {
        public static Hashtable list = new Hashtable();
        public static Hashtable db_list = new Hashtable();
        public static bool updated_db = false;
        public static bool updated_ws = false;

        public static Label o_sync_status;

        public static void sync()
        {
            Hashtable tmp = new Hashtable();
            foreach (DictionaryEntry entry in list)
            {
                Product product = entry.Value as Product;
                product.action = "";
                if (!db_list.ContainsKey(product.codigo))
                {
                    product.action = "d";
                }
                tmp[product.codigo] = product;
            }
            foreach (DictionaryEntry entry in db_list)
            {
                Product product = entry.Value as Product;
                product.action = "";
                if (!list.ContainsKey(product.codigo))
                {
                    product.action = "a";
                }
                else
                {
                    Product l_product = list[product.codigo.ToString()] as Product;
                    if (!l_product.tipo_codigo.Equals(product.tipo_codigo) && product.action.Equals("")) { product.action = "u"; }
                    if (!l_product.nome.Equals(product.nome) && product.action.Equals("")) { product.action = "u"; }
                    if (!l_product.preco.Equals(product.preco) && product.action.Equals("")) { product.action = "u"; }
                    if (!l_product.pvalsiva.Equals(product.pvalsiva) && product.action.Equals("")) { product.action = "u"; }
                    if (!l_product.pvp3siva.Equals(product.pvp3siva) && product.action.Equals("")) { product.action = "u"; }
                    if (!l_product.bonus.Equals(product.bonus) && product.action.Equals("")) { product.action = "u"; }
                    if (!l_product.iva.Equals(product.iva) && product.action.Equals("")) { product.action = "u"; }
                    if (!l_product.familia.Equals(product.familia) && product.action.Equals("")) { product.action = "u"; }
                }
                tmp[product.codigo] = product;
            }
            list = new Hashtable();
            db_list = new Hashtable();
            db_list = tmp;
            return;
        }

        public static bool update()
        {
            if (Products.updated_db)
            {
                int counter = 0;
                string products_str = "";
                foreach (DictionaryEntry entry in Products.db_list)
                {
                    Product product = entry.Value as Product;
                    if (product.action.Equals("")) continue;
                    if (counter == 0) products_str += "&products=";
                    else products_str += "_spout_";
                    products_str += WebUtility.UrlEncode(product.codigo) + "_spin_";
                    products_str += WebUtility.UrlEncode(product.nome) + "_spin_";
                    products_str += WebUtility.UrlEncode(product.tipo_codigo) + "_spin_";
                    products_str += WebUtility.UrlEncode(product.preco) + "_spin_";
                    products_str += WebUtility.UrlEncode(product.bonus) + "_spin_";
                    products_str += WebUtility.UrlEncode(product.iva) + "_spin_";
                    products_str += WebUtility.UrlEncode(product.familia) + "_spin_";
                    products_str += WebUtility.UrlEncode(product.pvalsiva) + "_spin_";
                    products_str += WebUtility.UrlEncode(product.pvp3siva) + "_spin_";
                    products_str += WebUtility.UrlEncode(product.action);
                    counter += 1;
                }
                if (!updated_ws)
                {
                    products_str += "&initial_update=1";
                    updated_ws = true;
                }
                if (counter != 0)
                {
                    //Utils.log(products_str);
                    string retval = ws.post("products", products_str);
                    if (retval != "error")
                    {
                        list = new Hashtable();
                        foreach (DictionaryEntry entry in Products.db_list)
                        {
                            Product product = entry.Value as Product;
                            if (product.action.Equals("d")) continue;
                            list[product.codigo.ToString()] = product;
                        }
                        o_sync_status.Text = counter + " produtos sincronizados";
                        return true;
                    }
                    else
                    {
                        o_sync_status.Text = "Erro ao enviar para o servidor";
                        return false;
                    }
                }
                else
                {
                    list = db_list;
                    o_sync_status.Text = "Produtos sem alterações";
                    return true;
                }
            }
            else
            {
                o_sync_status.Text = "Base de dados não atualizada";
                return false;
            }
            return true;
        }
    }
}
