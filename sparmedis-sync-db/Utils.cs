﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sparmedis_sync_db
{
    public class Utils
    {
        public static TextBox o_log;

        public static bool ping(string url, bool http = false)
        {
            if(http)
            {
                try
                {
                    HttpWebRequest reqFP = (HttpWebRequest)HttpWebRequest.Create(url);
                    HttpWebResponse rspFP = (HttpWebResponse)reqFP.GetResponse();
                    if (HttpStatusCode.OK == rspFP.StatusCode)
                    {
                        // HTTP = 200 - Internet connection available, server online
                        rspFP.Close();
                        return true;
                    }
                    else
                    {
                        // Other status - Server or connection not available
                        rspFP.Close();
                        return false;
                    }
                }
                catch (WebException e)
                {
                    // Exception - connection not available
                    return false;
                }
            }
            else
            {
                Ping pingSender = new Ping();
                PingOptions options = new PingOptions();

                // Use the default Ttl value which is 128, 
                // but change the fragmentation behavior.
                options.DontFragment = true;

                // Create a buffer of 32 bytes of data to be transmitted. 
                string data = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
                byte[] buffer = Encoding.ASCII.GetBytes(data);
                int timeout = 120;
                try { 
                    PingReply reply = pingSender.Send(url, timeout, buffer, options);
                    if (reply.Status == IPStatus.Success)
                    {
                        return true;
                    }
                }
                catch (Exception e)
                {
                    return false;
                }

                
                return false;
            }
        }
        public static void log(string message)
        {
            o_log.AppendText("[" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "] " + message + "\n");
        }
    }
}
