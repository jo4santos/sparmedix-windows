﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sparmedis_sync_db
{
    public class db
    {
        public static main o_main;
        public static LinkLabel o_status;
        public static Label o_status_label;
        public static bool status = false;
        public static SqlConnection conn = null;
        static int test_interval = 3600;
        static Timer test_timer = new Timer();

        public static bool init()
        {
            test_timer.Tick += new EventHandler(refresh_connection); // Everytime timer ticks, timer_Tick will be called
            test_timer.Interval = (test_interval * 1000) * (1);              // Timer will tick evert second
            test_timer.Enabled = true;                       // Enable the timer
            test_timer.Start();
            refresh_connection(null,null);
            return true;
        }

        public static void update()
        {
            update_clients();
            Clients.sync();
            update_products();
            Products.sync();
        }

        public static void refresh_connection(object sender, EventArgs e)
        {
            string log_message = "Testar ligação à base de dados ... ";
            /*if (!Utils.ping(sparmedis_sync_db.Properties.Settings.Default.bd_ip))
            {
                log_message += "Erro, servidor não responde";
                status = false;
            }
            else
            {*/
                string connectionString = "Data Source=";
                //connectionString += "127.0.0.1,1433;";
                //connectionString += "SRV_SP\\SqlExpress;";
                connectionString += sparmedis_sync_db.Properties.Settings.Default.bd_ip + ";";
                connectionString += "Initial Catalog="+sparmedis_sync_db.Properties.Settings.Default.bd_name + ";";
                connectionString += "User Id=" + sparmedis_sync_db.Properties.Settings.Default.bd_username + ";Password=" + sparmedis_sync_db.Properties.Settings.Default.bd_password + ";";

                try
                {
                    conn = new SqlConnection(connectionString);
                    conn.Open();
                }
                catch (Exception ex)
                {
                    log_message += ex.Message;
                    log_message += "Erro, base de dados não acessível";
                    if (conn != null)
                    {
                        conn.Dispose();
                        conn = null;
                    }
                }

                status = (conn != null && conn.State == ConnectionState.Open);
            //}
            o_status_label.Text = "Em " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString() + ":";
            if (status)
            {
                log_message += "OK";
                o_status.Text = "Activo";
                o_status.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            }
            else
            {
                o_status.Text = "Inactivo";
                o_status.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            }
            Utils.log(log_message);
        }
        public static void update_clients()
        {
            Clients.updated_db = false;
            Clients.db_list = new Hashtable();
            if (status)
            {
                //string sql = @"select * from " + sparmedis_sync_db.Properties.Settings.Default.db_field_client_tablename + " ORDER BY '" + sparmedis_sync_db.Properties.Settings.Default.db_field_client_codigo + "' ASC";
                string sql = @"select * from " + sparmedis_sync_db.Properties.Settings.Default.db_field_client_tablename + " WHERE inactivo='0' ORDER BY '" + sparmedis_sync_db.Properties.Settings.Default.db_field_client_codigo + "' ASC";

                try
                {
                    SqlDataAdapter da = new SqlDataAdapter(sql, conn);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    int counter = 0;
                    foreach (DataRow row in dt.Rows)
                    {
                        Client o_client = new Client();
                        o_client.id = counter++.ToString();
                        o_client.codigo = row[sparmedis_sync_db.Properties.Settings.Default.db_field_client_codigo].ToString().Trim();
                        o_client.nome = row[sparmedis_sync_db.Properties.Settings.Default.db_field_client_nome].ToString().Trim();
                        o_client.nif = row[sparmedis_sync_db.Properties.Settings.Default.db_field_client_nif].ToString().Trim();
                        o_client.responsavel = row[sparmedis_sync_db.Properties.Settings.Default.db_field_client_responsavel].ToString().Trim();
                        o_client.morada = row[sparmedis_sync_db.Properties.Settings.Default.db_field_client_morada].ToString().Trim();
                        o_client.email = row[sparmedis_sync_db.Properties.Settings.Default.db_field_client_email].ToString().Trim();
                        o_client.local = row[sparmedis_sync_db.Properties.Settings.Default.db_field_client_local].ToString().Trim();
                        o_client.cdpostal = row[sparmedis_sync_db.Properties.Settings.Default.db_field_client_cdpostal].ToString().Trim();
                        o_client.tppreco = row[sparmedis_sync_db.Properties.Settings.Default.db_field_client_tppreco].ToString().Trim();

                        if (o_client.nome.Equals("") || o_client.codigo.Equals(""))
                        {
                            continue;
                        }

                        Clients.db_list[row[sparmedis_sync_db.Properties.Settings.Default.db_field_client_codigo].ToString()] = o_client;
                    }
                    Clients.updated_db = true;
                }
                catch (Exception e)
                {
                    Utils.log("Erro ao obter clientes da base de dados: " + e.GetType().Name + " | " + e.Message);
                    Clients.updated_db = false;
                    return;
                }
                Clients.updated_db = true;
            }
            return;
        }
        public static void update_products()
        {
            Products.updated_db = false;
            Products.db_list = new Hashtable();
            if (status)
            {
                string sql = @"select * from " + sparmedis_sync_db.Properties.Settings.Default.db_field_product_tablename+ " WHERE inactivo='0' ORDER BY '" + sparmedis_sync_db.Properties.Settings.Default.db_field_product_codigo + "' ASC";
                //string sql = @"select * from " + sparmedis_sync_db.Properties.Settings.Default.db_field_product_tablename + " ORDER BY '" + sparmedis_sync_db.Properties.Settings.Default.db_field_product_codigo + "' ASC";

                try
                {
                    SqlDataAdapter da = new SqlDataAdapter(sql, conn);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    int counter = 0;
                    foreach (DataRow row in dt.Rows)
                    {
                        Product o_product = new Product();
                        o_product.id = counter++.ToString();
                        o_product.codigo = row[sparmedis_sync_db.Properties.Settings.Default.db_field_product_codigo].ToString().Trim();
                        int n;
                        bool isNumeric = int.TryParse(o_product.codigo, out n);
                        if (isNumeric && o_product.codigo.Length == 13)
                        {
                            o_product.tipo_codigo = "ean";
                        }
                        else if (isNumeric && o_product.codigo.Length == 7)
                        {
                            o_product.tipo_codigo = "cnp";
                        }
                        else
                        {
                            o_product.tipo_codigo = "cod";
                        }
                        o_product.nome = row[sparmedis_sync_db.Properties.Settings.Default.db_field_product_nome].ToString().Trim();
                        o_product.preco = row[sparmedis_sync_db.Properties.Settings.Default.db_field_product_preco].ToString().Trim();
                        o_product.pvalsiva = row[sparmedis_sync_db.Properties.Settings.Default.db_field_product_pvalsiva].ToString().Trim();
                        o_product.pvp3siva = row[sparmedis_sync_db.Properties.Settings.Default.db_field_product_pvp3siva].ToString().Trim();
                        o_product.bonus = row[sparmedis_sync_db.Properties.Settings.Default.db_field_product_bonus].ToString().Trim();
                        int codigo_iva = Int32.Parse(row[sparmedis_sync_db.Properties.Settings.Default.db_field_product_iva].ToString().Trim());
                        if (codigo_iva == Int32.Parse(sparmedis_sync_db.Properties.Settings.Default.db_field_IVA_codigo_isento))
                        {
                            o_product.iva = sparmedis_sync_db.Properties.Settings.Default.db_field_IVA_valor_isento;
                        }
                        else if (codigo_iva == Int32.Parse(sparmedis_sync_db.Properties.Settings.Default.db_field_IVA_codigo_reduzido))
                        {
                            o_product.iva = sparmedis_sync_db.Properties.Settings.Default.db_field_IVA_valor_reduzido;
                        }
                        else if (codigo_iva == Int32.Parse(sparmedis_sync_db.Properties.Settings.Default.db_field_IVA_codigo_intermedio))
                        {
                            o_product.iva = sparmedis_sync_db.Properties.Settings.Default.db_field_IVA_valor_intermedio;
                        }
                        else if (codigo_iva == Int32.Parse(sparmedis_sync_db.Properties.Settings.Default.db_field_IVA_codigo_normal))
                        {
                            o_product.iva = sparmedis_sync_db.Properties.Settings.Default.db_field_IVA_valor_normal;
                        }
                        o_product.familia = row[sparmedis_sync_db.Properties.Settings.Default.db_field_product_familia].ToString().Trim();

                        if (o_product.nome.Equals("") || o_product.codigo.Equals("") || o_product.preco.Equals("") || o_product.iva.Equals(""))
                        {
                            continue;
                        }

                        Products.db_list[row[sparmedis_sync_db.Properties.Settings.Default.db_field_product_codigo].ToString()] = o_product;
                    }
                    Products.updated_db = true;
                }
                catch (Exception e)
                {
                    Utils.log("Erro ao obter produtos da base de dados: " + e.GetType().Name + " | " + e.Message);
                    Products.updated_db = false;
                    return;
                }
                Products.updated_db = true;
            }
            return;
        }





        /*public static void fill()
        {
            string sql = "";
            sql = "DELETE FROM Produtos";
            try
            {
                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                Utils.log("Erro ao apagar produtos da base de dados: " + e.GetType().Name);
                return;
            }
            sql = "DELETE FROM Clientes";
            try
            {
                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                Utils.log("Erro ao apagar clientes da base de dados: " + e.GetType().Name);
                return;
            }
            sql = "INSERT INTO Produtos (tipo_codigo,codigo,nome,preco,bonus,iva) VALUES (@tipo_codigo, @codigo, @nome, @preco, @bonus, @iva)";
            try
            {
                string[] possiblecodes = new string[2] { "EAN", "CNP" };
                for (var i = 0; i < 5; i++)
                {
                    Random random = new Random();
                    int j = random.Next(2);
                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.Parameters.AddWithValue("@tipo_codigo", possiblecodes[j]);
                    cmd.Parameters.AddWithValue("@codigo", "12300" + i);
                    cmd.Parameters.AddWithValue("@nome", RandomString(2, true) + " nome do produto " + i);
                    random = new Random();
                    cmd.Parameters.AddWithValue("@preco", random.Next(1, 100) + ".00");
                    random = new Random();
                    cmd.Parameters.AddWithValue("@bonus", random.Next(5) * 10);
                    random = new Random();
                    cmd.Parameters.AddWithValue("@iva", random.Next(30));
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                Utils.log("Erro ao inserir produto na base de dados: " + e.GetType().Name);
                return;
            }
            sql = "INSERT INTO Clientes (codigo,nome,nif,responsavel,morada,email) VALUES (@codigo, @nome, @nif, @responsavel, @morada, @email)";
            try
            {
                for (var i = 0; i < 3; i++)
                {
                    Random random = new Random();
                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.Parameters.AddWithValue("@codigo", "123" + i);
                    cmd.Parameters.AddWithValue("@nome", RandomString(2, true) + " nome de cliente " + i);
                    cmd.Parameters.AddWithValue("@nif", 1234 + i);
                    cmd.Parameters.AddWithValue("@responsavel", "Responsavel " + i);
                    cmd.Parameters.AddWithValue("@morada", "Morada " + i);
                    cmd.Parameters.AddWithValue("@email", "email"+i+"@mail.com");
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                Utils.log("Erro ao inserir clientes na base de dados: " + e.GetType().Name);
                return;
            }
        }

        public static string RandomString(int size, bool lowerCase)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }
            if (lowerCase)
                return builder.ToString().ToLower();
            return builder.ToString();
        }*/
    }
}
