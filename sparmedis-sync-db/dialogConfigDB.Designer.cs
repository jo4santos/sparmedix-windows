﻿namespace sparmedis_sync_db
{
    partial class dialogConfigDB
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.inputBDNAME = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.inputBDIP = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.inputBDPassword = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.inputBDUsername = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label23 = new System.Windows.Forms.Label();
            this.inputDbFieldProductPVP3SIVA = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.inputDbFieldProductPVALSIVA = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.inputDbFieldProductFAMILIA = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.inputDbFieldProductIVA = new System.Windows.Forms.TextBox();
            this.inputDbFieldProductBONUS = new System.Windows.Forms.TextBox();
            this.inputDbFieldProductPRECO = new System.Windows.Forms.TextBox();
            this.inputDbFieldProductNOME = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.inputDbNameProducts = new System.Windows.Forms.TextBox();
            this.inputDbFieldProductCODIGO = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label21 = new System.Windows.Forms.Label();
            this.inputDbFieldClientTPPRECO = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.inputDbFieldClientCDPOSTAL = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.inputDbFieldClientLOCAL = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.inputDbFieldClientEMAIL = new System.Windows.Forms.TextBox();
            this.inputDbFieldClientMORADA = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.inputDbNameClient = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.inputDbFieldClientCODIGO = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.inputDbFieldClientRESPONSAVEL = new System.Windows.Forms.TextBox();
            this.inputDbFieldClientNOME = new System.Windows.Forms.TextBox();
            this.inputDbFieldClientNIF = new System.Windows.Forms.TextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.inputDbIVA_valor_normal = new System.Windows.Forms.TextBox();
            this.inputDbIVA_valor_intermedio = new System.Windows.Forms.TextBox();
            this.inputDbIVA_valor_isento = new System.Windows.Forms.TextBox();
            this.inputDbIVA_valor_reduzido = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.inputDbIVA_codigo_normal = new System.Windows.Forms.TextBox();
            this.inputDbIVA_codigo_intermedio = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.inputDbIVA_codigo_isento = new System.Windows.Forms.TextBox();
            this.inputDbIVA_codigo_reduzido = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 30);
            this.label1.Margin = new System.Windows.Forms.Padding(15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "IP Servidor";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.inputBDNAME);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.inputBDIP);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(24, 24);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(15);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(5);
            this.groupBox1.Size = new System.Drawing.Size(200, 90);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Ligação";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // inputBDNAME
            // 
            this.inputBDNAME.Location = new System.Drawing.Point(85, 58);
            this.inputBDNAME.Margin = new System.Windows.Forms.Padding(15);
            this.inputBDNAME.Name = "inputBDNAME";
            this.inputBDNAME.Size = new System.Drawing.Size(100, 20);
            this.inputBDNAME.TabIndex = 2;
            this.inputBDNAME.Text = global::sparmedis_sync_db.Properties.Settings.Default.bd_ip;
            this.inputBDNAME.TextChanged += new System.EventHandler(this.inputBDNAME_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 60);
            this.label4.Margin = new System.Windows.Forms.Padding(15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Nome DB";
            // 
            // inputBDIP
            // 
            this.inputBDIP.Location = new System.Drawing.Point(85, 28);
            this.inputBDIP.Margin = new System.Windows.Forms.Padding(15);
            this.inputBDIP.Name = "inputBDIP";
            this.inputBDIP.Size = new System.Drawing.Size(100, 20);
            this.inputBDIP.TabIndex = 0;
            this.inputBDIP.Text = global::sparmedis_sync_db.Properties.Settings.Default.bd_ip;
            this.inputBDIP.TextChanged += new System.EventHandler(this.inputBDIP_TextChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.inputBDPassword);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.inputBDUsername);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Location = new System.Drawing.Point(239, 24);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(24);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(5);
            this.groupBox2.Size = new System.Drawing.Size(200, 90);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Autenticação";
            this.groupBox2.Enter += new System.EventHandler(this.groupBox2_Enter);
            // 
            // inputBDPassword
            // 
            this.inputBDPassword.Location = new System.Drawing.Point(85, 58);
            this.inputBDPassword.Margin = new System.Windows.Forms.Padding(15);
            this.inputBDPassword.Name = "inputBDPassword";
            this.inputBDPassword.Size = new System.Drawing.Size(100, 20);
            this.inputBDPassword.TabIndex = 2;
            this.inputBDPassword.UseSystemPasswordChar = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 60);
            this.label3.Margin = new System.Windows.Forms.Padding(15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Password";
            // 
            // inputBDUsername
            // 
            this.inputBDUsername.Location = new System.Drawing.Point(85, 28);
            this.inputBDUsername.Margin = new System.Windows.Forms.Padding(15);
            this.inputBDUsername.Name = "inputBDUsername";
            this.inputBDUsername.Size = new System.Drawing.Size(100, 20);
            this.inputBDUsername.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 30);
            this.label2.Margin = new System.Windows.Forms.Padding(15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Username";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(259, 432);
            this.button1.Margin = new System.Windows.Forms.Padding(15);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 25);
            this.button1.TabIndex = 6;
            this.button1.Text = "Aplicar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(364, 432);
            this.button2.Margin = new System.Windows.Forms.Padding(15);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 25);
            this.button2.TabIndex = 7;
            this.button2.Text = "Cancelar";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label23);
            this.groupBox3.Controls.Add(this.inputDbFieldProductPVP3SIVA);
            this.groupBox3.Controls.Add(this.label22);
            this.groupBox3.Controls.Add(this.inputDbFieldProductPVALSIVA);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.inputDbFieldProductFAMILIA);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.inputDbFieldProductIVA);
            this.groupBox3.Controls.Add(this.inputDbFieldProductBONUS);
            this.groupBox3.Controls.Add(this.inputDbFieldProductPRECO);
            this.groupBox3.Controls.Add(this.inputDbFieldProductNOME);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.inputDbNameProducts);
            this.groupBox3.Controls.Add(this.inputDbFieldProductCODIGO);
            this.groupBox3.Location = new System.Drawing.Point(24, 144);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(15);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(5);
            this.groupBox3.Size = new System.Drawing.Size(200, 280);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Correspondências tabela produtos";
            this.groupBox3.Enter += new System.EventHandler(this.groupBox3_Enter);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(5, 232);
            this.label23.Margin = new System.Windows.Forms.Padding(15);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(61, 13);
            this.label23.TabIndex = 12;
            this.label23.Text = "PVP3 SIVA";
            // 
            // inputDbFieldProductPVP3SIVA
            // 
            this.inputDbFieldProductPVP3SIVA.Location = new System.Drawing.Point(79, 229);
            this.inputDbFieldProductPVP3SIVA.Margin = new System.Windows.Forms.Padding(15);
            this.inputDbFieldProductPVP3SIVA.Name = "inputDbFieldProductPVP3SIVA";
            this.inputDbFieldProductPVP3SIVA.Size = new System.Drawing.Size(100, 20);
            this.inputDbFieldProductPVP3SIVA.TabIndex = 11;
            this.inputDbFieldProductPVP3SIVA.Text = "pvp3_sem_iva";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(5, 207);
            this.label22.Margin = new System.Windows.Forms.Padding(15);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(56, 13);
            this.label22.TabIndex = 10;
            this.label22.Text = "PVal SIVA";
            // 
            // inputDbFieldProductPVALSIVA
            // 
            this.inputDbFieldProductPVALSIVA.Location = new System.Drawing.Point(79, 204);
            this.inputDbFieldProductPVALSIVA.Margin = new System.Windows.Forms.Padding(15);
            this.inputDbFieldProductPVALSIVA.Name = "inputDbFieldProductPVALSIVA";
            this.inputDbFieldProductPVALSIVA.Size = new System.Drawing.Size(100, 20);
            this.inputDbFieldProductPVALSIVA.TabIndex = 9;
            this.inputDbFieldProductPVALSIVA.Text = "pvalsiva";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(6, 182);
            this.label18.Margin = new System.Windows.Forms.Padding(15);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(41, 13);
            this.label18.TabIndex = 8;
            this.label18.Text = "Família";
            // 
            // inputDbFieldProductFAMILIA
            // 
            this.inputDbFieldProductFAMILIA.Location = new System.Drawing.Point(80, 179);
            this.inputDbFieldProductFAMILIA.Margin = new System.Windows.Forms.Padding(15);
            this.inputDbFieldProductFAMILIA.Name = "inputDbFieldProductFAMILIA";
            this.inputDbFieldProductFAMILIA.Size = new System.Drawing.Size(99, 20);
            this.inputDbFieldProductFAMILIA.TabIndex = 7;
            this.inputDbFieldProductFAMILIA.Text = "familia";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 157);
            this.label9.Margin = new System.Windows.Forms.Padding(15);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(24, 13);
            this.label9.TabIndex = 6;
            this.label9.Text = "IVA";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 132);
            this.label8.Margin = new System.Windows.Forms.Padding(15);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(37, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "Bónus";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 107);
            this.label7.Margin = new System.Windows.Forms.Padding(15);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "PVP SIVA";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 82);
            this.label6.Margin = new System.Windows.Forms.Padding(15);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Nome";
            // 
            // inputDbFieldProductIVA
            // 
            this.inputDbFieldProductIVA.Location = new System.Drawing.Point(80, 154);
            this.inputDbFieldProductIVA.Margin = new System.Windows.Forms.Padding(15);
            this.inputDbFieldProductIVA.Name = "inputDbFieldProductIVA";
            this.inputDbFieldProductIVA.Size = new System.Drawing.Size(100, 20);
            this.inputDbFieldProductIVA.TabIndex = 5;
            this.inputDbFieldProductIVA.Text = "iva";
            // 
            // inputDbFieldProductBONUS
            // 
            this.inputDbFieldProductBONUS.Location = new System.Drawing.Point(80, 129);
            this.inputDbFieldProductBONUS.Margin = new System.Windows.Forms.Padding(15);
            this.inputDbFieldProductBONUS.Name = "inputDbFieldProductBONUS";
            this.inputDbFieldProductBONUS.Size = new System.Drawing.Size(100, 20);
            this.inputDbFieldProductBONUS.TabIndex = 5;
            this.inputDbFieldProductBONUS.Text = "bonus";
            // 
            // inputDbFieldProductPRECO
            // 
            this.inputDbFieldProductPRECO.Location = new System.Drawing.Point(80, 104);
            this.inputDbFieldProductPRECO.Margin = new System.Windows.Forms.Padding(15);
            this.inputDbFieldProductPRECO.Name = "inputDbFieldProductPRECO";
            this.inputDbFieldProductPRECO.Size = new System.Drawing.Size(100, 20);
            this.inputDbFieldProductPRECO.TabIndex = 5;
            this.inputDbFieldProductPRECO.Text = "pvpsiva";
            // 
            // inputDbFieldProductNOME
            // 
            this.inputDbFieldProductNOME.Location = new System.Drawing.Point(80, 79);
            this.inputDbFieldProductNOME.Margin = new System.Windows.Forms.Padding(15);
            this.inputDbFieldProductNOME.Name = "inputDbFieldProductNOME";
            this.inputDbFieldProductNOME.Size = new System.Drawing.Size(100, 20);
            this.inputDbFieldProductNOME.TabIndex = 5;
            this.inputDbFieldProductNOME.Text = "nome";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 32);
            this.label17.Margin = new System.Windows.Forms.Padding(15);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(71, 13);
            this.label17.TabIndex = 4;
            this.label17.Text = "Nome Tabela";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 57);
            this.label5.Margin = new System.Windows.Forms.Padding(15);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Código";
            // 
            // inputDbNameProducts
            // 
            this.inputDbNameProducts.Location = new System.Drawing.Point(80, 29);
            this.inputDbNameProducts.Margin = new System.Windows.Forms.Padding(15);
            this.inputDbNameProducts.Name = "inputDbNameProducts";
            this.inputDbNameProducts.Size = new System.Drawing.Size(100, 20);
            this.inputDbNameProducts.TabIndex = 2;
            this.inputDbNameProducts.Text = "artigos";
            // 
            // inputDbFieldProductCODIGO
            // 
            this.inputDbFieldProductCODIGO.Location = new System.Drawing.Point(80, 54);
            this.inputDbFieldProductCODIGO.Margin = new System.Windows.Forms.Padding(15);
            this.inputDbFieldProductCODIGO.Name = "inputDbFieldProductCODIGO";
            this.inputDbFieldProductCODIGO.Size = new System.Drawing.Size(100, 20);
            this.inputDbFieldProductCODIGO.TabIndex = 2;
            this.inputDbFieldProductCODIGO.Text = "codigo";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label21);
            this.groupBox4.Controls.Add(this.inputDbFieldClientTPPRECO);
            this.groupBox4.Controls.Add(this.label20);
            this.groupBox4.Controls.Add(this.inputDbFieldClientCDPOSTAL);
            this.groupBox4.Controls.Add(this.label19);
            this.groupBox4.Controls.Add(this.inputDbFieldClientLOCAL);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this.inputDbFieldClientEMAIL);
            this.groupBox4.Controls.Add(this.inputDbFieldClientMORADA);
            this.groupBox4.Controls.Add(this.label11);
            this.groupBox4.Controls.Add(this.label12);
            this.groupBox4.Controls.Add(this.label13);
            this.groupBox4.Controls.Add(this.inputDbNameClient);
            this.groupBox4.Controls.Add(this.label15);
            this.groupBox4.Controls.Add(this.inputDbFieldClientCODIGO);
            this.groupBox4.Controls.Add(this.label14);
            this.groupBox4.Controls.Add(this.inputDbFieldClientRESPONSAVEL);
            this.groupBox4.Controls.Add(this.inputDbFieldClientNOME);
            this.groupBox4.Controls.Add(this.inputDbFieldClientNIF);
            this.groupBox4.Location = new System.Drawing.Point(239, 144);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(15);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(5);
            this.groupBox4.Size = new System.Drawing.Size(200, 280);
            this.groupBox4.TabIndex = 6;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Correspondências tabela clientes";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(11, 257);
            this.label21.Margin = new System.Windows.Forms.Padding(15);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(58, 13);
            this.label21.TabIndex = 24;
            this.label21.Text = "Tipo preço";
            // 
            // inputDbFieldClientTPPRECO
            // 
            this.inputDbFieldClientTPPRECO.Location = new System.Drawing.Point(85, 254);
            this.inputDbFieldClientTPPRECO.Margin = new System.Windows.Forms.Padding(15);
            this.inputDbFieldClientTPPRECO.Name = "inputDbFieldClientTPPRECO";
            this.inputDbFieldClientTPPRECO.Size = new System.Drawing.Size(99, 20);
            this.inputDbFieldClientTPPRECO.TabIndex = 23;
            this.inputDbFieldClientTPPRECO.Text = "tppreco";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(12, 232);
            this.label20.Margin = new System.Windows.Forms.Padding(15);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(72, 13);
            this.label20.TabIndex = 22;
            this.label20.Text = "Código Postal";
            // 
            // inputDbFieldClientCDPOSTAL
            // 
            this.inputDbFieldClientCDPOSTAL.Location = new System.Drawing.Point(86, 229);
            this.inputDbFieldClientCDPOSTAL.Margin = new System.Windows.Forms.Padding(15);
            this.inputDbFieldClientCDPOSTAL.Name = "inputDbFieldClientCDPOSTAL";
            this.inputDbFieldClientCDPOSTAL.Size = new System.Drawing.Size(99, 20);
            this.inputDbFieldClientCDPOSTAL.TabIndex = 21;
            this.inputDbFieldClientCDPOSTAL.Text = "cdpostal";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(12, 207);
            this.label19.Margin = new System.Windows.Forms.Padding(15);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(59, 13);
            this.label19.TabIndex = 20;
            this.label19.Text = "Localidade";
            // 
            // inputDbFieldClientLOCAL
            // 
            this.inputDbFieldClientLOCAL.Location = new System.Drawing.Point(86, 204);
            this.inputDbFieldClientLOCAL.Margin = new System.Windows.Forms.Padding(15);
            this.inputDbFieldClientLOCAL.Name = "inputDbFieldClientLOCAL";
            this.inputDbFieldClientLOCAL.Size = new System.Drawing.Size(99, 20);
            this.inputDbFieldClientLOCAL.TabIndex = 19;
            this.inputDbFieldClientLOCAL.Text = "local";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(11, 182);
            this.label16.Margin = new System.Windows.Forms.Padding(15);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(35, 13);
            this.label16.TabIndex = 15;
            this.label16.Text = "E-mail";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(11, 157);
            this.label10.Margin = new System.Windows.Forms.Padding(15);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(43, 13);
            this.label10.TabIndex = 15;
            this.label10.Text = "Morada";
            // 
            // inputDbFieldClientEMAIL
            // 
            this.inputDbFieldClientEMAIL.Location = new System.Drawing.Point(85, 179);
            this.inputDbFieldClientEMAIL.Margin = new System.Windows.Forms.Padding(15);
            this.inputDbFieldClientEMAIL.Name = "inputDbFieldClientEMAIL";
            this.inputDbFieldClientEMAIL.Size = new System.Drawing.Size(100, 20);
            this.inputDbFieldClientEMAIL.TabIndex = 11;
            this.inputDbFieldClientEMAIL.Text = "email";
            // 
            // inputDbFieldClientMORADA
            // 
            this.inputDbFieldClientMORADA.Location = new System.Drawing.Point(85, 154);
            this.inputDbFieldClientMORADA.Margin = new System.Windows.Forms.Padding(15);
            this.inputDbFieldClientMORADA.Name = "inputDbFieldClientMORADA";
            this.inputDbFieldClientMORADA.Size = new System.Drawing.Size(100, 20);
            this.inputDbFieldClientMORADA.TabIndex = 11;
            this.inputDbFieldClientMORADA.Text = "morada";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(11, 132);
            this.label11.Margin = new System.Windows.Forms.Padding(15);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(69, 13);
            this.label11.TabIndex = 16;
            this.label11.Text = "Responsável";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(11, 107);
            this.label12.Margin = new System.Windows.Forms.Padding(15);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(24, 13);
            this.label12.TabIndex = 17;
            this.label12.Text = "NIF";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(11, 82);
            this.label13.Margin = new System.Windows.Forms.Padding(15);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(35, 13);
            this.label13.TabIndex = 18;
            this.label13.Text = "Nome";
            // 
            // inputDbNameClient
            // 
            this.inputDbNameClient.Location = new System.Drawing.Point(85, 29);
            this.inputDbNameClient.Margin = new System.Windows.Forms.Padding(15);
            this.inputDbNameClient.Name = "inputDbNameClient";
            this.inputDbNameClient.Size = new System.Drawing.Size(100, 20);
            this.inputDbNameClient.TabIndex = 7;
            this.inputDbNameClient.Text = "clientes";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(11, 32);
            this.label15.Margin = new System.Windows.Forms.Padding(15);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(71, 13);
            this.label15.TabIndex = 10;
            this.label15.Text = "Nome Tabela";
            // 
            // inputDbFieldClientCODIGO
            // 
            this.inputDbFieldClientCODIGO.Location = new System.Drawing.Point(85, 54);
            this.inputDbFieldClientCODIGO.Margin = new System.Windows.Forms.Padding(15);
            this.inputDbFieldClientCODIGO.Name = "inputDbFieldClientCODIGO";
            this.inputDbFieldClientCODIGO.Size = new System.Drawing.Size(100, 20);
            this.inputDbFieldClientCODIGO.TabIndex = 7;
            this.inputDbFieldClientCODIGO.Text = "codigo";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(11, 57);
            this.label14.Margin = new System.Windows.Forms.Padding(15);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(40, 13);
            this.label14.TabIndex = 10;
            this.label14.Text = "Código";
            // 
            // inputDbFieldClientRESPONSAVEL
            // 
            this.inputDbFieldClientRESPONSAVEL.Location = new System.Drawing.Point(85, 129);
            this.inputDbFieldClientRESPONSAVEL.Margin = new System.Windows.Forms.Padding(15);
            this.inputDbFieldClientRESPONSAVEL.Name = "inputDbFieldClientRESPONSAVEL";
            this.inputDbFieldClientRESPONSAVEL.Size = new System.Drawing.Size(100, 20);
            this.inputDbFieldClientRESPONSAVEL.TabIndex = 12;
            this.inputDbFieldClientRESPONSAVEL.Text = "responsavel";
            // 
            // inputDbFieldClientNOME
            // 
            this.inputDbFieldClientNOME.Location = new System.Drawing.Point(85, 79);
            this.inputDbFieldClientNOME.Margin = new System.Windows.Forms.Padding(15);
            this.inputDbFieldClientNOME.Name = "inputDbFieldClientNOME";
            this.inputDbFieldClientNOME.Size = new System.Drawing.Size(100, 20);
            this.inputDbFieldClientNOME.TabIndex = 14;
            this.inputDbFieldClientNOME.Text = "nome";
            // 
            // inputDbFieldClientNIF
            // 
            this.inputDbFieldClientNIF.Location = new System.Drawing.Point(85, 104);
            this.inputDbFieldClientNIF.Margin = new System.Windows.Forms.Padding(15);
            this.inputDbFieldClientNIF.Name = "inputDbFieldClientNIF";
            this.inputDbFieldClientNIF.Size = new System.Drawing.Size(100, 20);
            this.inputDbFieldClientNIF.TabIndex = 13;
            this.inputDbFieldClientNIF.Text = "nif";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.inputDbIVA_valor_normal);
            this.groupBox5.Controls.Add(this.inputDbIVA_valor_intermedio);
            this.groupBox5.Controls.Add(this.inputDbIVA_valor_isento);
            this.groupBox5.Controls.Add(this.inputDbIVA_valor_reduzido);
            this.groupBox5.Controls.Add(this.label24);
            this.groupBox5.Controls.Add(this.label25);
            this.groupBox5.Controls.Add(this.inputDbIVA_codigo_normal);
            this.groupBox5.Controls.Add(this.inputDbIVA_codigo_intermedio);
            this.groupBox5.Controls.Add(this.label26);
            this.groupBox5.Controls.Add(this.label27);
            this.groupBox5.Controls.Add(this.inputDbIVA_codigo_isento);
            this.groupBox5.Controls.Add(this.inputDbIVA_codigo_reduzido);
            this.groupBox5.Location = new System.Drawing.Point(454, 144);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(15);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(5);
            this.groupBox5.Size = new System.Drawing.Size(200, 134);
            this.groupBox5.TabIndex = 9;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Configuração IVA";
            // 
            // inputDbIVA_valor_normal
            // 
            this.inputDbIVA_valor_normal.Location = new System.Drawing.Point(150, 107);
            this.inputDbIVA_valor_normal.Margin = new System.Windows.Forms.Padding(15);
            this.inputDbIVA_valor_normal.Name = "inputDbIVA_valor_normal";
            this.inputDbIVA_valor_normal.Size = new System.Drawing.Size(40, 20);
            this.inputDbIVA_valor_normal.TabIndex = 9;
            this.inputDbIVA_valor_normal.Text = "23";
            // 
            // inputDbIVA_valor_intermedio
            // 
            this.inputDbIVA_valor_intermedio.Location = new System.Drawing.Point(150, 82);
            this.inputDbIVA_valor_intermedio.Margin = new System.Windows.Forms.Padding(15);
            this.inputDbIVA_valor_intermedio.Name = "inputDbIVA_valor_intermedio";
            this.inputDbIVA_valor_intermedio.Size = new System.Drawing.Size(40, 20);
            this.inputDbIVA_valor_intermedio.TabIndex = 10;
            this.inputDbIVA_valor_intermedio.Text = "12";
            // 
            // inputDbIVA_valor_isento
            // 
            this.inputDbIVA_valor_isento.Location = new System.Drawing.Point(150, 32);
            this.inputDbIVA_valor_isento.Margin = new System.Windows.Forms.Padding(15);
            this.inputDbIVA_valor_isento.Name = "inputDbIVA_valor_isento";
            this.inputDbIVA_valor_isento.Size = new System.Drawing.Size(40, 20);
            this.inputDbIVA_valor_isento.TabIndex = 7;
            this.inputDbIVA_valor_isento.Text = "2";
            // 
            // inputDbIVA_valor_reduzido
            // 
            this.inputDbIVA_valor_reduzido.Location = new System.Drawing.Point(150, 57);
            this.inputDbIVA_valor_reduzido.Margin = new System.Windows.Forms.Padding(15);
            this.inputDbIVA_valor_reduzido.Name = "inputDbIVA_valor_reduzido";
            this.inputDbIVA_valor_reduzido.Size = new System.Drawing.Size(40, 20);
            this.inputDbIVA_valor_reduzido.TabIndex = 8;
            this.inputDbIVA_valor_reduzido.Text = "6";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(6, 107);
            this.label24.Margin = new System.Windows.Forms.Padding(15);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(40, 13);
            this.label24.TabIndex = 6;
            this.label24.Text = "Normal";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(6, 82);
            this.label25.Margin = new System.Windows.Forms.Padding(15);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(56, 13);
            this.label25.TabIndex = 6;
            this.label25.Text = "Intermédio";
            // 
            // inputDbIVA_codigo_normal
            // 
            this.inputDbIVA_codigo_normal.Location = new System.Drawing.Point(100, 107);
            this.inputDbIVA_codigo_normal.Margin = new System.Windows.Forms.Padding(15);
            this.inputDbIVA_codigo_normal.Name = "inputDbIVA_codigo_normal";
            this.inputDbIVA_codigo_normal.Size = new System.Drawing.Size(40, 20);
            this.inputDbIVA_codigo_normal.TabIndex = 5;
            this.inputDbIVA_codigo_normal.Text = "4";
            // 
            // inputDbIVA_codigo_intermedio
            // 
            this.inputDbIVA_codigo_intermedio.Location = new System.Drawing.Point(100, 82);
            this.inputDbIVA_codigo_intermedio.Margin = new System.Windows.Forms.Padding(15);
            this.inputDbIVA_codigo_intermedio.Name = "inputDbIVA_codigo_intermedio";
            this.inputDbIVA_codigo_intermedio.Size = new System.Drawing.Size(40, 20);
            this.inputDbIVA_codigo_intermedio.TabIndex = 5;
            this.inputDbIVA_codigo_intermedio.Text = "3";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(6, 32);
            this.label26.Margin = new System.Windows.Forms.Padding(15);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(36, 13);
            this.label26.TabIndex = 4;
            this.label26.Text = "Isento";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(6, 57);
            this.label27.Margin = new System.Windows.Forms.Padding(15);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(52, 13);
            this.label27.TabIndex = 4;
            this.label27.Text = "Reduzido";
            // 
            // inputDbIVA_codigo_isento
            // 
            this.inputDbIVA_codigo_isento.Location = new System.Drawing.Point(100, 32);
            this.inputDbIVA_codigo_isento.Margin = new System.Windows.Forms.Padding(15);
            this.inputDbIVA_codigo_isento.Name = "inputDbIVA_codigo_isento";
            this.inputDbIVA_codigo_isento.Size = new System.Drawing.Size(40, 20);
            this.inputDbIVA_codigo_isento.TabIndex = 2;
            this.inputDbIVA_codigo_isento.Text = "1";
            // 
            // inputDbIVA_codigo_reduzido
            // 
            this.inputDbIVA_codigo_reduzido.Location = new System.Drawing.Point(100, 57);
            this.inputDbIVA_codigo_reduzido.Margin = new System.Windows.Forms.Padding(15);
            this.inputDbIVA_codigo_reduzido.Name = "inputDbIVA_codigo_reduzido";
            this.inputDbIVA_codigo_reduzido.Size = new System.Drawing.Size(40, 20);
            this.inputDbIVA_codigo_reduzido.TabIndex = 2;
            this.inputDbIVA_codigo_reduzido.Text = "2";
            // 
            // dialogConfigDB
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(677, 478);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "dialogConfigDB";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Configuração da BD";
            this.TopMost = true;
            this.Shown += new System.EventHandler(this.dialogConfigDB_Shown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox inputBDIP;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox inputBDUsername;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox inputBDPassword;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox inputDbFieldProductIVA;
        private System.Windows.Forms.TextBox inputDbFieldProductBONUS;
        private System.Windows.Forms.TextBox inputDbFieldProductPRECO;
        private System.Windows.Forms.TextBox inputDbFieldProductNOME;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox inputDbFieldProductCODIGO;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox inputDbFieldClientEMAIL;
        private System.Windows.Forms.TextBox inputDbFieldClientMORADA;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox inputDbFieldClientCODIGO;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox inputDbFieldClientRESPONSAVEL;
        private System.Windows.Forms.TextBox inputDbFieldClientNOME;
        private System.Windows.Forms.TextBox inputDbFieldClientNIF;
        private System.Windows.Forms.TextBox inputBDNAME;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox inputDbNameProducts;
        private System.Windows.Forms.TextBox inputDbNameClient;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox inputDbFieldProductFAMILIA;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox inputDbFieldClientCDPOSTAL;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox inputDbFieldClientLOCAL;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox inputDbIVA_valor_normal;
        private System.Windows.Forms.TextBox inputDbIVA_valor_intermedio;
        private System.Windows.Forms.TextBox inputDbIVA_valor_isento;
        private System.Windows.Forms.TextBox inputDbIVA_valor_reduzido;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox inputDbIVA_codigo_normal;
        private System.Windows.Forms.TextBox inputDbIVA_codigo_intermedio;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox inputDbIVA_codigo_isento;
        private System.Windows.Forms.TextBox inputDbIVA_codigo_reduzido;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox inputDbFieldClientTPPRECO;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox inputDbFieldProductPVP3SIVA;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox inputDbFieldProductPVALSIVA;
    }
}