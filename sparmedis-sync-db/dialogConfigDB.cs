﻿using sparmedis_sync_db.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sparmedis_sync_db
{
    public partial class dialogConfigDB : Form
    {
        public dialogConfigDB()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            sparmedis_sync_db.Properties.Settings.Default.bd_ip = inputBDIP.Text;
            sparmedis_sync_db.Properties.Settings.Default.bd_name = inputBDNAME.Text;
            sparmedis_sync_db.Properties.Settings.Default.bd_username = inputBDUsername.Text;
            sparmedis_sync_db.Properties.Settings.Default.bd_password = inputBDPassword.Text;

            sparmedis_sync_db.Properties.Settings.Default.db_field_product_tablename = inputDbNameProducts.Text;
            sparmedis_sync_db.Properties.Settings.Default.db_field_product_codigo = inputDbFieldProductCODIGO.Text;
            sparmedis_sync_db.Properties.Settings.Default.db_field_product_nome = inputDbFieldProductNOME.Text;
            sparmedis_sync_db.Properties.Settings.Default.db_field_product_preco = inputDbFieldProductPRECO.Text;
            sparmedis_sync_db.Properties.Settings.Default.db_field_product_bonus = inputDbFieldProductBONUS.Text;
            sparmedis_sync_db.Properties.Settings.Default.db_field_product_iva = inputDbFieldProductIVA.Text;
            sparmedis_sync_db.Properties.Settings.Default.db_field_product_familia = inputDbFieldProductFAMILIA.Text;
            sparmedis_sync_db.Properties.Settings.Default.db_field_product_pvalsiva = inputDbFieldProductPVALSIVA.Text;
            sparmedis_sync_db.Properties.Settings.Default.db_field_product_pvp3siva = inputDbFieldProductPVP3SIVA.Text;

            sparmedis_sync_db.Properties.Settings.Default.db_field_client_tablename = inputDbNameClient.Text;
            sparmedis_sync_db.Properties.Settings.Default.db_field_client_codigo = inputDbFieldClientCODIGO.Text;
            sparmedis_sync_db.Properties.Settings.Default.db_field_client_nome = inputDbFieldClientNOME.Text;
            sparmedis_sync_db.Properties.Settings.Default.db_field_client_nif = inputDbFieldClientNIF.Text;
            sparmedis_sync_db.Properties.Settings.Default.db_field_client_responsavel = inputDbFieldClientRESPONSAVEL.Text;
            sparmedis_sync_db.Properties.Settings.Default.db_field_client_morada = inputDbFieldClientMORADA.Text;
            sparmedis_sync_db.Properties.Settings.Default.db_field_client_email = inputDbFieldClientEMAIL.Text;
            sparmedis_sync_db.Properties.Settings.Default.db_field_client_local = inputDbFieldClientLOCAL.Text;
            sparmedis_sync_db.Properties.Settings.Default.db_field_client_cdpostal = inputDbFieldClientCDPOSTAL.Text;
            sparmedis_sync_db.Properties.Settings.Default.db_field_client_tppreco = inputDbFieldClientTPPRECO.Text;

            sparmedis_sync_db.Properties.Settings.Default.db_field_IVA_codigo_isento = inputDbIVA_codigo_isento.Text;
            sparmedis_sync_db.Properties.Settings.Default.db_field_IVA_codigo_reduzido = inputDbIVA_codigo_reduzido.Text;
            sparmedis_sync_db.Properties.Settings.Default.db_field_IVA_codigo_intermedio = inputDbIVA_codigo_intermedio.Text;
            sparmedis_sync_db.Properties.Settings.Default.db_field_IVA_codigo_normal = inputDbIVA_codigo_normal.Text;

            sparmedis_sync_db.Properties.Settings.Default.db_field_IVA_valor_isento = inputDbIVA_valor_isento.Text;
            sparmedis_sync_db.Properties.Settings.Default.db_field_IVA_valor_reduzido = inputDbIVA_valor_reduzido.Text;
            sparmedis_sync_db.Properties.Settings.Default.db_field_IVA_valor_intermedio = inputDbIVA_valor_intermedio.Text;
            sparmedis_sync_db.Properties.Settings.Default.db_field_IVA_valor_normal = inputDbIVA_valor_normal.Text;

            sparmedis_sync_db.Properties.Settings.Default.Save();
            db.refresh_connection(null, null);
            this.Close();
        }

        private void dialogConfigDB_Shown(object sender, EventArgs e)
        {
            inputBDIP.Text = sparmedis_sync_db.Properties.Settings.Default.bd_ip;
            inputBDNAME.Text = sparmedis_sync_db.Properties.Settings.Default.bd_name;
            inputBDUsername.Text = sparmedis_sync_db.Properties.Settings.Default.bd_username;
            inputBDPassword.Text = sparmedis_sync_db.Properties.Settings.Default.bd_password;

            inputDbNameProducts.Text = sparmedis_sync_db.Properties.Settings.Default.db_field_product_tablename;
            inputDbFieldProductCODIGO.Text = sparmedis_sync_db.Properties.Settings.Default.db_field_product_codigo;
            inputDbFieldProductNOME.Text = sparmedis_sync_db.Properties.Settings.Default.db_field_product_nome;
            inputDbFieldProductPRECO.Text = sparmedis_sync_db.Properties.Settings.Default.db_field_product_preco;
            inputDbFieldProductBONUS.Text = sparmedis_sync_db.Properties.Settings.Default.db_field_product_bonus;
            inputDbFieldProductIVA.Text = sparmedis_sync_db.Properties.Settings.Default.db_field_product_iva;
            inputDbFieldProductFAMILIA.Text = sparmedis_sync_db.Properties.Settings.Default.db_field_product_familia;
            inputDbFieldProductPVALSIVA.Text = sparmedis_sync_db.Properties.Settings.Default.db_field_product_pvalsiva;
            inputDbFieldProductPVP3SIVA.Text = sparmedis_sync_db.Properties.Settings.Default.db_field_product_pvp3siva;

            inputDbNameClient.Text = sparmedis_sync_db.Properties.Settings.Default.db_field_client_tablename;
            inputDbFieldClientCODIGO.Text = sparmedis_sync_db.Properties.Settings.Default.db_field_client_codigo;
            inputDbFieldClientNOME.Text = sparmedis_sync_db.Properties.Settings.Default.db_field_client_nome;
            inputDbFieldClientNIF.Text = sparmedis_sync_db.Properties.Settings.Default.db_field_client_nif;
            inputDbFieldClientRESPONSAVEL.Text = sparmedis_sync_db.Properties.Settings.Default.db_field_client_responsavel;
            inputDbFieldClientMORADA.Text = sparmedis_sync_db.Properties.Settings.Default.db_field_client_morada;
            inputDbFieldClientEMAIL.Text = sparmedis_sync_db.Properties.Settings.Default.db_field_client_email;
            inputDbFieldClientLOCAL.Text = sparmedis_sync_db.Properties.Settings.Default.db_field_client_local;
            inputDbFieldClientCDPOSTAL.Text = sparmedis_sync_db.Properties.Settings.Default.db_field_client_cdpostal;
            inputDbFieldClientTPPRECO.Text = sparmedis_sync_db.Properties.Settings.Default.db_field_client_tppreco;

            inputDbIVA_codigo_isento.Text = sparmedis_sync_db.Properties.Settings.Default.db_field_IVA_codigo_isento;
            inputDbIVA_codigo_reduzido.Text = sparmedis_sync_db.Properties.Settings.Default.db_field_IVA_codigo_reduzido;
            inputDbIVA_codigo_intermedio.Text = sparmedis_sync_db.Properties.Settings.Default.db_field_IVA_codigo_intermedio;
            inputDbIVA_codigo_normal.Text = sparmedis_sync_db.Properties.Settings.Default.db_field_IVA_codigo_normal;

            inputDbIVA_valor_isento.Text = sparmedis_sync_db.Properties.Settings.Default.db_field_IVA_valor_isento;
            inputDbIVA_valor_reduzido.Text = sparmedis_sync_db.Properties.Settings.Default.db_field_IVA_valor_reduzido;
            inputDbIVA_valor_intermedio.Text = sparmedis_sync_db.Properties.Settings.Default.db_field_IVA_valor_intermedio;
            inputDbIVA_valor_normal.Text = sparmedis_sync_db.Properties.Settings.Default.db_field_IVA_valor_normal;
        }

        private void groupBox3_Enter(object sender, EventArgs e)
        {

        }

        private void inputBDIP_TextChanged(object sender, EventArgs e)
        {

        }

        private void inputBDNAME_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
