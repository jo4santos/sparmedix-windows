﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sparmedis_sync_db
{
    public partial class dialogConfigWeb : Form
    {
        public dialogConfigWeb()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            sparmedis_sync_db.Properties.Settings.Default.web_key = inputWebKey.Text;
            sparmedis_sync_db.Properties.Settings.Default.web_url = inputWebURL.Text;
            sparmedis_sync_db.Properties.Settings.Default.Save();
            ws.refresh_connection(null, null);
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dialogConfigWeb_Shown(object sender, EventArgs e)
        {
            inputWebURL.Text = sparmedis_sync_db.Properties.Settings.Default.web_url;
            inputWebKey.Text = sparmedis_sync_db.Properties.Settings.Default.web_key;
        }

        private void label4_Click(object sender, EventArgs e)
        {
            inputWebURL.Text = "http://sparmedis.pt/ws/";
        }
    }
}
