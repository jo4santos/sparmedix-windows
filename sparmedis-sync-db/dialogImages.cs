﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sparmedis_sync_db
{
    public partial class dialogImages : Form
    {
        public dialogImages()
        {
            string path = @Application.StartupPath + "\\imagens";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            InitializeComponent();

            updateExisting();


            this.AllowDrop = true;
            this.DragEnter += new DragEventHandler(Images_DragEnter);
            this.DragDrop += new DragEventHandler(Images_DragDrop);
        }
        void Images_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop)) e.Effect = DragDropEffects.Copy;
        }

        void Images_DragDrop(object sender, DragEventArgs e)
        {
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
            foreach (string file in files)
            {
                System.IO.FileInfo fileinfo = new System.IO.FileInfo(file);
                if (IsValidImage(file))
                {
                    if (ProductExists(Path.GetFileNameWithoutExtension(file)))
                    {
                        ListImagensNovas.Items.Add(file);
                    }
                }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            while (ListImagensNovas.CheckedItems.Count > 0)
            {
                ListImagensNovas.Items.Remove(ListImagensNovas.CheckedItems[0]);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
             ListImagensNovas.Items.Clear();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            while (ListImagensNovas.Items.Count > 0)
            {
                string fileName = System.IO.Path.GetFileName(ListImagensNovas.Items[0].ToString());
                string targetPath = @Application.StartupPath+"\\imagens";

                // Use Path class to manipulate file and directory paths. 
                string sourceFile = ListImagensNovas.Items[0].ToString();
                string destFile = System.IO.Path.Combine(targetPath, fileName);

                // To copy a folder's contents to a new location: 
                // Create a new target folder, if necessary. 
                if (!System.IO.Directory.Exists(targetPath))
                {
                    System.IO.Directory.CreateDirectory(targetPath);
                }

                // To copy a file to another location and  
                // overwrite the destination file if it already exists.
                //System.IO.File.Copy(sourceFile, destFile, true);

                ResizeImage(sourceFile, destFile, 200, 200, true);

                ListImagensNovas.Items.Remove(ListImagensNovas.Items[0]);
            }
            updateExisting();
        }
        bool IsValidImage(string filename)
        {
            try
            {
                Image newImage = Image.FromFile(filename);
                newImage.Dispose();
            }
            catch (OutOfMemoryException ex)
            {
                // Image.FromFile will throw this if file is invalid.
                // Don't ask me why.
                return false;
            }
            return true;
        }
        bool ProductExists(string codigo)
        {
            foreach (DictionaryEntry entry in Products.db_list)
            {
                Product product = entry.Value as Product;
                if (product.codigo.Equals(codigo)) return true;
            }
            return false;
        }

        private void ListImagensNovas_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ListImagensNovas.Items.Count > 0 && ListImagensNovas.SelectedIndex >= 0)
            {
                FileStream fileStream = new FileStream(@ListImagensNovas.SelectedItem.ToString(), FileMode.Open);
                pictureBox1.Image = System.Drawing.Image.FromStream(fileStream);
                fileStream.Close();
            }
        }

        private void ListImagensExistentes_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ListImagensExistentes.Items.Count > 0 && ListImagensExistentes.SelectedIndex >= 0)
            {
                FileStream fileStream = new FileStream(@Application.StartupPath + "\\imagens\\" + ListImagensExistentes.SelectedItem.ToString(), FileMode.Open);
                pictureBox2.Image = System.Drawing.Image.FromStream(fileStream);
                fileStream.Close();
            }
        }
        public void ResizeImage(string OriginalFile, string NewFile, int NewWidth, int MaxHeight, bool OnlyResizeIfWider)
        {
            System.Drawing.Image FullsizeImage = System.Drawing.Image.FromFile(OriginalFile);

            // Prevent using images internal thumbnail
            FullsizeImage.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone);
            FullsizeImage.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone);

            if (OnlyResizeIfWider)
            {
                if (FullsizeImage.Width <= NewWidth)
                {
                    NewWidth = FullsizeImage.Width;
                }
            }

            int NewHeight = FullsizeImage.Height * NewWidth / FullsizeImage.Width;
            if (NewHeight > MaxHeight)
            {
                // Resize with height instead
                NewWidth = FullsizeImage.Width * MaxHeight / FullsizeImage.Height;
                NewHeight = MaxHeight;
            }

            System.Drawing.Image NewImage = FullsizeImage.GetThumbnailImage(NewWidth, NewHeight, null, IntPtr.Zero);

            // Clear handle to original file so that we can overwrite it if necessary
            FullsizeImage.Dispose();

            // Save resized picture
            NewImage.Save(NewFile);
            NewImage.Dispose();
        }
        private void updateExisting()
        {
            ListImagensExistentes.Items.Clear();
            string[] fileEntries = Directory.GetFiles(Application.StartupPath + "\\imagens");

            System.IO.StreamWriter file = new System.IO.StreamWriter(@Application.StartupPath + "\\imagens\\status.txt");

            foreach (string fileName in fileEntries)
            {
                System.IO.FileInfo fileinfo = new System.IO.FileInfo(fileName);
                if (IsValidImage(fileName))
                {
                    if (ProductExists(Path.GetFileNameWithoutExtension(fileName)))
                    {
                        file.WriteLine(fileinfo.Name + "," + fileinfo.LastWriteTime + "," + fileinfo.Length);
                        ListImagensExistentes.Items.Add(Path.GetFileName(fileName));
                    }
                }
            }
            file.Dispose();
            return;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            while (ListImagensExistentes.CheckedItems.Count > 0)
            {
                File.Delete(Application.StartupPath + "\\imagens\\" + ListImagensExistentes.CheckedItems[0].ToString());
                ListImagensExistentes.Items.Remove(ListImagensExistentes.CheckedItems[0]);
            }
            updateExisting();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            string startPath = @Application.StartupPath + "\\imagens";
            string zipPath = @Application.StartupPath + "\\imagens.zip";

            File.Delete(zipPath);

            ZipFile.CreateFromDirectory(startPath, zipPath);

            NameValueCollection nvc = new NameValueCollection();
            nvc.Add("chave", sparmedis_sync_db.Properties.Settings.Default.web_key);
            bool retval = ws.upload_file(sparmedis_sync_db.Properties.Settings.Default.web_url + "images", @Application.StartupPath + "\\imagens.zip", "file", "application/octet-stream", nvc);
            if (retval)
            {
                MessageBox.Show("Imagens enviadas para o servidor web com sucesso");
            }
            else
            {
                MessageBox.Show("Erro ao enviar imagens para o servidor. Tente novamente, se o erro persistir, contacte o suporte: js@josesantos.eu");
            }

            File.Delete(zipPath);

            return;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            string startPath = @Application.StartupPath + "\\imagens";
            string zipPath = @Application.StartupPath + "\\imagens.zip";

            File.Delete(zipPath);

            WebClient webClient = new WebClient();
            webClient.DownloadFile(sparmedis_sync_db.Properties.Settings.Default.web_url + "../areacliente/images/imagens.zip", @zipPath);

            System.IO.DirectoryInfo imagesFolderInfo = new DirectoryInfo(startPath);

            foreach (FileInfo file in imagesFolderInfo.GetFiles())
            {
                file.Delete();
            }

            ZipFile.ExtractToDirectory(zipPath, startPath);
            updateExisting();
        }
    }
}
