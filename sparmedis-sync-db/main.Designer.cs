﻿namespace sparmedis_sync_db
{
    partial class main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(main));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.configuraçãoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.baseDeDadosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.servidorWebToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ajudaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.syncLog = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.wsConnStatus = new System.Windows.Forms.LinkLabel();
            this.wsConnStatusLabel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dbConnStatus = new System.Windows.Forms.LinkLabel();
            this.dbConnStatusLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.syncContainerNext = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.syncInterval = new System.Windows.Forms.ComboBox();
            this.nextSyncTime = new System.Windows.Forms.Label();
            this.btnSyncNow = new System.Windows.Forms.Button();
            this.syncContainerLast = new System.Windows.Forms.GroupBox();
            this.productsSyncStatus = new System.Windows.Forms.Label();
            this.clientsSyncStatus = new System.Windows.Forms.Label();
            this.lastSyncTime = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.currentTime = new System.Windows.Forms.Label();
            this.lastSyncStatus = new System.Windows.Forms.LinkLabel();
            this.productsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.clientsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.menuStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.syncContainerNext.SuspendLayout();
            this.syncContainerLast.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.productsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.Menu;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.configuraçãoToolStripMenuItem,
            this.ajudaToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(763, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // configuraçãoToolStripMenuItem
            // 
            this.configuraçãoToolStripMenuItem.BackColor = System.Drawing.SystemColors.MenuBar;
            this.configuraçãoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.baseDeDadosToolStripMenuItem,
            this.servidorWebToolStripMenuItem});
            this.configuraçãoToolStripMenuItem.Name = "configuraçãoToolStripMenuItem";
            this.configuraçãoToolStripMenuItem.Size = new System.Drawing.Size(91, 20);
            this.configuraçãoToolStripMenuItem.Text = "Configuração";
            // 
            // baseDeDadosToolStripMenuItem
            // 
            this.baseDeDadosToolStripMenuItem.Name = "baseDeDadosToolStripMenuItem";
            this.baseDeDadosToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.baseDeDadosToolStripMenuItem.Text = "Base de dados";
            this.baseDeDadosToolStripMenuItem.Click += new System.EventHandler(this.baseDeDadosToolStripMenuItem_Click);
            // 
            // servidorWebToolStripMenuItem
            // 
            this.servidorWebToolStripMenuItem.Name = "servidorWebToolStripMenuItem";
            this.servidorWebToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.servidorWebToolStripMenuItem.Text = "Servidor Web";
            this.servidorWebToolStripMenuItem.Click += new System.EventHandler(this.servidorWebToolStripMenuItem_Click);
            // 
            // ajudaToolStripMenuItem
            // 
            this.ajudaToolStripMenuItem.Name = "ajudaToolStripMenuItem";
            this.ajudaToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.ajudaToolStripMenuItem.Text = "Ajuda";
            // 
            // syncLog
            // 
            this.syncLog.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.syncLog.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.syncLog.Location = new System.Drawing.Point(12, 116);
            this.syncLog.Multiline = true;
            this.syncLog.Name = "syncLog";
            this.syncLog.ReadOnly = true;
            this.syncLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.syncLog.Size = new System.Drawing.Size(476, 157);
            this.syncLog.TabIndex = 2;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.syncLog);
            this.panel1.Controls.Add(this.syncContainerNext);
            this.panel1.Controls.Add(this.syncContainerLast);
            this.panel1.Location = new System.Drawing.Point(0, 24);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(500, 326);
            this.panel1.TabIndex = 1;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(347, 0);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(141, 23);
            this.button2.TabIndex = 4;
            this.button2.Text = "Sincronizar imagens";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.wsConnStatus);
            this.groupBox2.Controls.Add(this.wsConnStatusLabel);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.groupBox2.Location = new System.Drawing.Point(256, 276);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(232, 41);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Ligação ao servidor Web";
            // 
            // wsConnStatus
            // 
            this.wsConnStatus.ActiveLinkColor = System.Drawing.Color.White;
            this.wsConnStatus.AutoSize = true;
            this.wsConnStatus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.wsConnStatus.DisabledLinkColor = System.Drawing.Color.White;
            this.wsConnStatus.ForeColor = System.Drawing.Color.White;
            this.wsConnStatus.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.wsConnStatus.LinkColor = System.Drawing.Color.White;
            this.wsConnStatus.Location = new System.Drawing.Point(143, 18);
            this.wsConnStatus.Name = "wsConnStatus";
            this.wsConnStatus.Padding = new System.Windows.Forms.Padding(5, 2, 5, 2);
            this.wsConnStatus.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.wsConnStatus.Size = new System.Drawing.Size(47, 17);
            this.wsConnStatus.TabIndex = 4;
            this.wsConnStatus.TabStop = true;
            this.wsConnStatus.Text = "Activo";
            this.toolTip1.SetToolTip(this.wsConnStatus, "Clique para testar ligação");
            this.wsConnStatus.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.wsConnStatus_LinkClicked);
            // 
            // wsConnStatusLabel
            // 
            this.wsConnStatusLabel.AutoSize = true;
            this.wsConnStatusLabel.Location = new System.Drawing.Point(7, 20);
            this.wsConnStatusLabel.Name = "wsConnStatusLabel";
            this.wsConnStatusLabel.Size = new System.Drawing.Size(130, 13);
            this.wsConnStatusLabel.TabIndex = 3;
            this.wsConnStatusLabel.Text = "Em 27-01-2014 20:41:00: ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 13);
            this.label2.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dbConnStatus);
            this.groupBox1.Controls.Add(this.dbConnStatusLabel);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.groupBox1.Location = new System.Drawing.Point(12, 276);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(232, 41);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Ligação à base de dados";
            // 
            // dbConnStatus
            // 
            this.dbConnStatus.ActiveLinkColor = System.Drawing.Color.White;
            this.dbConnStatus.AutoSize = true;
            this.dbConnStatus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.dbConnStatus.DisabledLinkColor = System.Drawing.Color.White;
            this.dbConnStatus.ForeColor = System.Drawing.Color.White;
            this.dbConnStatus.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.dbConnStatus.LinkColor = System.Drawing.Color.White;
            this.dbConnStatus.Location = new System.Drawing.Point(143, 18);
            this.dbConnStatus.Name = "dbConnStatus";
            this.dbConnStatus.Padding = new System.Windows.Forms.Padding(5, 2, 5, 2);
            this.dbConnStatus.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.dbConnStatus.Size = new System.Drawing.Size(47, 17);
            this.dbConnStatus.TabIndex = 3;
            this.dbConnStatus.TabStop = true;
            this.dbConnStatus.Text = "Activo";
            this.toolTip1.SetToolTip(this.dbConnStatus, "Clique para testar ligação");
            this.dbConnStatus.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.dbConnStatus_LinkClicked);
            // 
            // dbConnStatusLabel
            // 
            this.dbConnStatusLabel.AutoSize = true;
            this.dbConnStatusLabel.Location = new System.Drawing.Point(7, 20);
            this.dbConnStatusLabel.Name = "dbConnStatusLabel";
            this.dbConnStatusLabel.Size = new System.Drawing.Size(130, 13);
            this.dbConnStatusLabel.TabIndex = 2;
            this.dbConnStatusLabel.Text = "Em 27-01-2014 20:41:00: ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 0;
            // 
            // syncContainerNext
            // 
            this.syncContainerNext.Controls.Add(this.label3);
            this.syncContainerNext.Controls.Add(this.syncInterval);
            this.syncContainerNext.Controls.Add(this.nextSyncTime);
            this.syncContainerNext.Controls.Add(this.btnSyncNow);
            this.syncContainerNext.Location = new System.Drawing.Point(256, 25);
            this.syncContainerNext.Margin = new System.Windows.Forms.Padding(0);
            this.syncContainerNext.Name = "syncContainerNext";
            this.syncContainerNext.Size = new System.Drawing.Size(232, 88);
            this.syncContainerNext.TabIndex = 1;
            this.syncContainerNext.TabStop = false;
            this.syncContainerNext.Text = "Próxima sincronização";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 42);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(140, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Tempo entre sincronizações";
            // 
            // syncInterval
            // 
            this.syncInterval.FormattingEnabled = true;
            this.syncInterval.Location = new System.Drawing.Point(154, 38);
            this.syncInterval.Name = "syncInterval";
            this.syncInterval.Size = new System.Drawing.Size(69, 21);
            this.syncInterval.TabIndex = 2;
            this.syncInterval.SelectedIndexChanged += new System.EventHandler(this.syncInterval_SelectedIndexChanged);
            // 
            // nextSyncTime
            // 
            this.nextSyncTime.AutoSize = true;
            this.nextSyncTime.Location = new System.Drawing.Point(7, 20);
            this.nextSyncTime.Name = "nextSyncTime";
            this.nextSyncTime.Size = new System.Drawing.Size(75, 13);
            this.nextSyncTime.TabIndex = 1;
            this.nextSyncTime.Text = "asdasdasdasd";
            // 
            // btnSyncNow
            // 
            this.btnSyncNow.Location = new System.Drawing.Point(-1, 63);
            this.btnSyncNow.Name = "btnSyncNow";
            this.btnSyncNow.Size = new System.Drawing.Size(233, 25);
            this.btnSyncNow.TabIndex = 0;
            this.btnSyncNow.Text = "Sincronizar agora";
            this.btnSyncNow.UseVisualStyleBackColor = true;
            this.btnSyncNow.Click += new System.EventHandler(this.btnSyncNow_Click);
            // 
            // syncContainerLast
            // 
            this.syncContainerLast.Controls.Add(this.productsSyncStatus);
            this.syncContainerLast.Controls.Add(this.clientsSyncStatus);
            this.syncContainerLast.Controls.Add(this.lastSyncTime);
            this.syncContainerLast.Location = new System.Drawing.Point(12, 25);
            this.syncContainerLast.Margin = new System.Windows.Forms.Padding(0);
            this.syncContainerLast.Name = "syncContainerLast";
            this.syncContainerLast.Size = new System.Drawing.Size(232, 88);
            this.syncContainerLast.TabIndex = 0;
            this.syncContainerLast.TabStop = false;
            this.syncContainerLast.Text = "Última sincronização";
            // 
            // productsSyncStatus
            // 
            this.productsSyncStatus.AutoSize = true;
            this.productsSyncStatus.Location = new System.Drawing.Point(7, 43);
            this.productsSyncStatus.Name = "productsSyncStatus";
            this.productsSyncStatus.Size = new System.Drawing.Size(31, 13);
            this.productsSyncStatus.TabIndex = 2;
            this.productsSyncStatus.Text = "        ";
            // 
            // clientsSyncStatus
            // 
            this.clientsSyncStatus.AutoSize = true;
            this.clientsSyncStatus.Location = new System.Drawing.Point(7, 66);
            this.clientsSyncStatus.Name = "clientsSyncStatus";
            this.clientsSyncStatus.Size = new System.Drawing.Size(31, 13);
            this.clientsSyncStatus.TabIndex = 1;
            this.clientsSyncStatus.Text = "        ";
            // 
            // lastSyncTime
            // 
            this.lastSyncTime.AutoSize = true;
            this.lastSyncTime.Location = new System.Drawing.Point(7, 20);
            this.lastSyncTime.Name = "lastSyncTime";
            this.lastSyncTime.Size = new System.Drawing.Size(31, 13);
            this.lastSyncTime.TabIndex = 0;
            this.lastSyncTime.Text = "        ";
            // 
            // toolTip1
            // 
            this.toolTip1.AutoPopDelay = 5000;
            this.toolTip1.InitialDelay = 200;
            this.toolTip1.ReshowDelay = 100;
            // 
            // currentTime
            // 
            this.currentTime.AutoSize = true;
            this.currentTime.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.currentTime.Location = new System.Drawing.Point(321, 9);
            this.currentTime.Name = "currentTime";
            this.currentTime.Size = new System.Drawing.Size(167, 13);
            this.currentTime.TabIndex = 2;
            this.currentTime.Text = "Hora actual: 01-01-2014 00:00:00";
            // 
            // lastSyncStatus
            // 
            this.lastSyncStatus.ActiveLinkColor = System.Drawing.Color.White;
            this.lastSyncStatus.AutoSize = true;
            this.lastSyncStatus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.lastSyncStatus.DisabledLinkColor = System.Drawing.Color.White;
            this.lastSyncStatus.ForeColor = System.Drawing.Color.White;
            this.lastSyncStatus.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.lastSyncStatus.LinkColor = System.Drawing.Color.White;
            this.lastSyncStatus.Location = new System.Drawing.Point(167, 66);
            this.lastSyncStatus.Name = "lastSyncStatus";
            this.lastSyncStatus.Padding = new System.Windows.Forms.Padding(5, 2, 5, 2);
            this.lastSyncStatus.Size = new System.Drawing.Size(32, 17);
            this.lastSyncStatus.TabIndex = 3;
            this.lastSyncStatus.TabStop = true;
            this.lastSyncStatus.Text = "OK";
            this.lastSyncStatus.Visible = false;
            this.lastSyncStatus.VisitedLinkColor = System.Drawing.Color.White;
            // 
            // productsBindingSource
            // 
            this.productsBindingSource.DataSource = typeof(sparmedis_sync_db.Products);
            // 
            // clientsBindingSource
            // 
            this.clientsBindingSource.DataSource = typeof(sparmedis_sync_db.Clients);
            // 
            // main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(763, 350);
            this.Controls.Add(this.lastSyncStatus);
            this.Controls.Add(this.currentTime);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "main";
            this.Text = "Sincronização de bases de dados";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.syncContainerNext.ResumeLayout(false);
            this.syncContainerNext.PerformLayout();
            this.syncContainerLast.ResumeLayout(false);
            this.syncContainerLast.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.productsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientsBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem configuraçãoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem baseDeDadosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem servidorWebToolStripMenuItem;
        public System.Windows.Forms.TextBox syncLog;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox syncContainerNext;
        private System.Windows.Forms.Button btnSyncNow;
        private System.Windows.Forms.GroupBox syncContainerLast;
        private System.Windows.Forms.Label nextSyncTime;
        private System.Windows.Forms.Label lastSyncTime;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label dbConnStatusLabel;
        private System.Windows.Forms.Label wsConnStatusLabel;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.LinkLabel dbConnStatus;
        private System.Windows.Forms.LinkLabel wsConnStatus;
        private System.Windows.Forms.Label currentTime;
        private System.Windows.Forms.BindingSource productsBindingSource;
        private System.Windows.Forms.BindingSource clientsBindingSource;
        private System.Windows.Forms.Label productsSyncStatus;
        private System.Windows.Forms.Label clientsSyncStatus;
        private System.Windows.Forms.LinkLabel lastSyncStatus;
        private System.Windows.Forms.ToolStripMenuItem ajudaToolStripMenuItem;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox syncInterval;
        private System.Windows.Forms.Button button2;

    }
}

