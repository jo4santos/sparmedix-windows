﻿using sparmedis_sync_db.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.IO;
using System.Data.SqlClient;
using System.Collections;

namespace sparmedis_sync_db
{
    public partial class main : Form
    {
        Timer update_timer = new Timer();
        Timer current_timer = new Timer();
        int update_interval = 30;
        bool initialized = false;
        public main()
        {
            InitializeComponent();

            Products.o_sync_status = productsSyncStatus;
            Clients.o_sync_status = clientsSyncStatus;
            Utils.o_log = syncLog;
            db.o_main = this;
            db.o_status = dbConnStatus;
            db.o_status_label = dbConnStatusLabel;
            ws.o_main = this;
            ws.o_status = wsConnStatus;
            ws.o_status_label = wsConnStatusLabel;

            syncInterval.Items.Add("1 minuto");
            syncInterval.Items.Add("30 minutos");
            syncInterval.Items.Add("1 hora");
            syncInterval.Items.Add("6 horas");
            syncInterval.Items.Add("12 horas");
            syncInterval.Items.Add("1 dia");
            syncInterval.Items.Add("1 semana");
            syncInterval.SelectedIndex=4;

            update_timer.Tick += new EventHandler(timer_Tick); // Everytime timer ticks, timer_Tick will be called
            update_timer.Interval = (update_interval * 1000) * (1);              // Timer will tick evert second
            update_timer.Enabled = true;                       // Enable the timer
            update_timer.Start();

            currentTime.Text = "Hora atual: " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
            current_timer.Tick += new EventHandler(current_timer_Tick); // Everytime timer ticks, timer_Tick will be called
            current_timer.Interval = (1000) * (1);              // Timer will tick evert second
            current_timer.Enabled = true;                       // Enable the timer
            current_timer.Start();

            db.init();
            ws.init();

            timer_Tick(null, null);

            this.initialized = true;
        }

        void timer_Tick(object sender, EventArgs e)
        {
            switch (syncInterval.SelectedIndex)
            {
                case 0:
                    {
                        update_interval = 60;
                        break;
                    }
                case 1:
                    {
                        update_interval = 60 * 30;
                        break;
                    }
                case 2:
                    {
                        update_interval = 60 * 60;
                        break;
                    }
                case 3:
                    {
                        update_interval = 60 * 60 * 6;
                        break;
                    }
                case 4:
                    {
                        update_interval = 60 * 60 * 12;
                        break;
                    }
                case 5:
                    {
                        update_interval = 60 * 60 * 24;
                        break;
                    }
                case 6:
                    {
                        update_interval = 60 * 60 * 24 * 7;
                        break;
                    }
                default:
                    {
                        update_interval = 12 * 60 * 60;
                        break;
                    }
            };
            update_timer.Stop();
            this.sync_all();
            lastSyncTime.Text = DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
            nextSyncTime.Text = DateTime.Now.AddSeconds(update_interval).ToShortDateString() + " " + DateTime.Now.AddSeconds(update_interval).ToLongTimeString();
            update_timer.Interval = (update_interval * 1000) * (1);              // Timer will tick evert second
            update_timer.Enabled = true;                       // Enable the timer
            update_timer.Start();
        }

        void current_timer_Tick(object sender, EventArgs e)
        {
            currentTime.Text = "Hora atual: " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
        }

        private void baseDeDadosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dialogConfigDB f = new dialogConfigDB();
            f.ShowDialog();
        }
        private void servidorWebToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dialogConfigWeb f = new dialogConfigWeb();
            f.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show(sparmedis_sync_db.Properties.Settings.Default.bd_ip);
        }
        private void btnSyncNow_Click(object sender, EventArgs e)
        {
            this.sync_all();
            return;
        }
        public Boolean sync_all()
        {
            if (!this.initialized) return false;
            btnSyncNow.Enabled = false;
            Utils.log("A iniciar sincronização");
            lastSyncTime.Text = DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
            if (db.status)
            {
                Utils.log("A obter informações da base de dados");

                db.update();

                if(ws.status)
                {
                    Utils.log("A enviar dados para o servidor Web");

                    bool syncstatus = true;
                    if (!Clients.update())
                    {
                        Utils.log("Erro na sincronização de clientes");
                        syncstatus = syncstatus && false;
                    }
                    if (!Products.update())
                    {
                        Utils.log("Erro na sincronização de produtos");
                        syncstatus = syncstatus && false;
                    }

                    if (syncstatus)
                    {
                        lastSyncStatus.Text = "Sucesso";
                        lastSyncStatus.Visible = true;
                        lastSyncStatus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
                    }
                    else
                    {
                        lastSyncStatus.Text = "Erro";
                        lastSyncStatus.Visible = true;
                        lastSyncStatus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                    }
                }
                else
                {
                    lastSyncStatus.Text = "Erro";
                    lastSyncStatus.Visible = true;
                    productsSyncStatus.Text = "-";
                    clientsSyncStatus.Text = "-";
                    lastSyncStatus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
                    Utils.log("Sincronização cancelada, servidor web inacessível");
            btnSyncNow.Enabled = true;
                    return false;
                }

                /*
                if (!Clients.update())
                {
                    this.log("Erro na sincronização de clientes");
                }
                if (!Products.update())
                {
                    this.log("Erro na sincronização de produtos");
                }
                */
            }
            else
            {
                Utils.log("Sincronização cancelada, base de dados inacessível");
                btnSyncNow.Enabled = true;
                return false;
            }
            Utils.log("Sincronização terminada com sucesso");
            btnSyncNow.Enabled = true;
            return true;
        }
        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            db.refresh_connection(null, null);
        }

        private void dbConnStatus_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            db.refresh_connection(null, null);
        }

        private void wsConnStatus_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ws.refresh_connection(null, null);
        }

        private void syncInterval_SelectedIndexChanged(object sender, EventArgs e)
        {
            timer_Tick(null, null);
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            //db.fill();
        }

        private void button2_Click(object sender, EventArgs e)
        {

            dialogImages f = new dialogImages();
            f.ShowDialog();
            return;
        }
    }
}
