﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sparmedis_sync_db
{
    public class ws
    {
        public static main o_main;
        public static LinkLabel o_status;
        public static Label o_status_label;
        public static bool status = false;
        static int test_interval = 3600;
        static Timer test_timer = new Timer();

        public static bool init()
        {
            test_timer.Tick += new EventHandler(refresh_connection); // Everytime timer ticks, timer_Tick will be called
            test_timer.Interval = (test_interval * 1000) * (1);              // Timer will tick evert second
            test_timer.Enabled = true;                       // Enable the timer
            test_timer.Start();
            refresh_connection(null, null);
            return true;
        }

        public static void refresh_connection(object sender, EventArgs e)
        {
            string log_message = "Testar ligação ao servidor Web ... ";

            string http_ip = sparmedis_sync_db.Properties.Settings.Default.web_url;
            http_ip = http_ip.Replace("http://", "");
            if (http_ip.IndexOf(':') != -1)
            {
                http_ip = http_ip.Substring(0, http_ip.IndexOf(':'));
            }
            if (http_ip.IndexOf('/') != -1)
            {
                http_ip = http_ip.Substring(0, http_ip.IndexOf('/'));
            }

            if (!Utils.ping(http_ip))
            {
                log_message += "Erro, servidor não responde";
                status = false;
            }
            else
            {
                status = true;
            }
            o_status_label.Text = "Em " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString() + ":";
            if (status)
            {
                log_message += "OK";
                o_status.Text = "Activo";
                o_status.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            }
            else
            {
                o_status.Text = "Inactivo";
                o_status.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            }
            Utils.log(log_message);
        }

        public static string post(string method, string data_str)
        {
            string url = sparmedis_sync_db.Properties.Settings.Default.web_url + method;
            var request = (HttpWebRequest)WebRequest.Create(url);
            var postData = "chave=" + sparmedis_sync_db.Properties.Settings.Default.web_key;
            postData += "&method=put";
            postData += data_str;
            var data = Encoding.ASCII.GetBytes(postData);

            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = data.Length;

            try
            {
                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
                return responseString;
            }
            catch (Exception ex)
            {
                Utils.log("Erro na ligação a " + url + " | " + ex.Message);
                return "error";
            }
        }

        /*NameValueCollection nvc = new NameValueCollection();
        nvc.Add("id", "TTR");
        nvc.Add("btn-submit-photo", "Upload");
        HttpUploadFile("http://your.server.com/upload", @"C:\test\test.jpg", "file", "image/jpeg", nvc);*/

        public static bool upload_file(string url, string file, string paramName, string contentType, NameValueCollection nvc)
        {
            //Utils.log(string.Format("Uploading {0} to {1}", file, url));
            string boundary = "---------------------------" + DateTime.Now.Ticks.ToString("x");
            byte[] boundarybytes = System.Text.Encoding.ASCII.GetBytes("\r\n--" + boundary + "\r\n");

            HttpWebRequest wr = (HttpWebRequest)WebRequest.Create(url);
            wr.ContentType = "multipart/form-data; boundary=" + boundary;
            wr.Method = "POST";
            wr.KeepAlive = true;
            wr.Credentials = System.Net.CredentialCache.DefaultCredentials;

            Stream rs;

            try
            {
                rs = wr.GetRequestStream();
            }
            catch (Exception ex)
            {
                Utils.log("Erro ao enviar imagens para o servidor. Tente novamente. | " + ex.Message);
                return false;
            }            

            string formdataTemplate = "Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}";
            foreach (string key in nvc.Keys)
            {
                rs.Write(boundarybytes, 0, boundarybytes.Length);
                string formitem = string.Format(formdataTemplate, key, nvc[key]);
                byte[] formitembytes = System.Text.Encoding.UTF8.GetBytes(formitem);
                rs.Write(formitembytes, 0, formitembytes.Length);
            }
            rs.Write(boundarybytes, 0, boundarybytes.Length);

            string headerTemplate = "Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"\r\nContent-Type: {2}\r\n\r\n";
            string header = string.Format(headerTemplate, paramName, file, contentType);
            byte[] headerbytes = System.Text.Encoding.UTF8.GetBytes(header);
            rs.Write(headerbytes, 0, headerbytes.Length);

            FileStream fileStream = new FileStream(file, FileMode.Open, FileAccess.Read);
            byte[] buffer = new byte[4096];
            int bytesRead = 0;
            while ((bytesRead = fileStream.Read(buffer, 0, buffer.Length)) != 0)
            {
                rs.Write(buffer, 0, bytesRead);
            }
            fileStream.Close();

            byte[] trailer = System.Text.Encoding.ASCII.GetBytes("\r\n--" + boundary + "--\r\n");
            rs.Write(trailer, 0, trailer.Length);
            rs.Close();

            WebResponse wresp = null;
            try
            {
                wresp = wr.GetResponse();
                Stream stream2 = wresp.GetResponseStream();
                StreamReader reader2 = new StreamReader(stream2);
                //Utils.log(string.Format("File uploaded, server response is: {0}", reader2.ReadToEnd()));
                return true;
            }
            catch (Exception ex)
            {
                Utils.log("Erro ao enviar imagens para o servidor. Tente novamente. | " + ex.Message);
                if (wresp != null)
                {
                    wresp.Close();
                    wresp = null;
                }
                return false;
            }
            finally
            {
                wr = null;
            }
            return false;
        }
    }
}
